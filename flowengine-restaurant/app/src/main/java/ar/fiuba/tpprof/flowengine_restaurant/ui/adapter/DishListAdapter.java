package ar.fiuba.tpprof.flowengine_restaurant.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.domain.meal.Dish;
import ar.fiuba.tpprof.flowengine_restaurant.domain.meal.DishSubcategory;
import ar.fiuba.tpprof.flowengine_restaurant.domain.meal.ListItem;


/**
 * Created by florencia on 06/09/15.
 */
public class DishListAdapter extends BaseAdapter {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;

    private Context context;
    private List<ListItem> dishes = new ArrayList();
    private TreeSet<Integer> sectionHeader = new TreeSet();

    private LayoutInflater inflater;

    public DishListAdapter(Context context, List<Dish> items) {
        this.context = context;
        setItems(items);
    }

    public void addItem(final Dish item) {
        dishes.add(item);
        notifyDataSetChanged();
    }

    public void addSectionHeaderItem(final DishSubcategory item) {
        dishes.add(item);
        sectionHeader.add(dishes.size() - 1);
        notifyDataSetChanged();
    }

    public void setItems(List<Dish> items){
        String currentSubcategory = "";
        for (Dish item: items){

            DishSubcategory subcategory = item.getDishSubcategory();
            if(subcategory != null && !currentSubcategory.equals(subcategory.getName())){
                addSectionHeaderItem(subcategory);
                currentSubcategory = subcategory.getName();
            }
            addItem(item);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return sectionHeader.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return dishes.size();
    }

    @Override
    public ListItem getItem(int position) {
        return dishes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View rowView, ViewGroup parent) {

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        int rowType = getItemViewType(position);
        switch (rowType) {
            case TYPE_ITEM:
                rowView = inflater.inflate(R.layout.dish_list_item , parent, false);
                setRowValues(position, rowView);
                break;
            case TYPE_SEPARATOR:
                rowView = inflater.inflate(R.layout.dish_subcategory_list_item , parent, false);
                setRowHeaderValues(position, rowView);
                break;
        }
        return rowView;
    }

    private void setRowValues(int position, View rowView) {

        Dish dish = (Dish)dishes.get(position);

        TextView txtDishName = (TextView) rowView.findViewById(R.id.dish_name);
        TextView txtDishCost = (TextView) rowView.findViewById(R.id.dish_cost);

        if (dish.getName() != null)
            txtDishName.setText(dish.getName());

        if (dish.getCost() != null){
            String cost = "$ " + dish.getCost().toString();
            txtDishCost.setText(cost);
        }

    }

    private void setRowHeaderValues(int position, View rowView) {

        DishSubcategory dishSubcategory = (DishSubcategory)dishes.get(position);

        TextView txtMainCourseLabel = (TextView) rowView.findViewById(R.id.dish_subcategory_name);

        if (dishSubcategory.getName() != null)
            txtMainCourseLabel.setText(dishSubcategory.getName());
    }

}
