package ar.fiuba.tpprof.flowengine_restaurant.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import java.util.Arrays;
import java.util.List;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.domain.meal.DishCategory;
import ar.fiuba.tpprof.flowengine_restaurant.ui.adapter.DishCategoryListAdapter;

public class DishCategorySelectionFragment extends TakingOrderFlowFragment {

    private ListView listDishCategoryView;
    private List<DishCategory> dishCategories;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setDishCategories();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dish_selection, container, false);
        buildListView(view);
        return view;
    }

    private void buildListView(View view){
        DishCategoryListAdapter adapter = new DishCategoryListAdapter(getActivity(), dishCategories);
        listDishCategoryView = (ListView) view.findViewById(R.id.list_dish_category);
        listDishCategoryView.setAdapter(adapter);

        listDishCategoryView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                DishCategory dishCategory = (DishCategory) listDishCategoryView.getItemAtPosition(position);
                setCategoryListener(dishCategory);
            }
        });
    }

    private void setCategoryListener(DishCategory dishCategory){
        switch (dishCategory){
            case MAIN_COURSE:
                takingOrderFlow.startMainCourseSequence();
                break;
            case BEVERAGE:
                takingOrderFlow.startBeverageSequence();
                break;
            case SNACK:
                takingOrderFlow.startSnackSequence();
                break;
            case DESSERT:
                takingOrderFlow.startDessertSequence();
                break;
        }
    }

    private void setDishCategories(){
        dishCategories = Arrays.asList(DishCategory.values());
    }

}
