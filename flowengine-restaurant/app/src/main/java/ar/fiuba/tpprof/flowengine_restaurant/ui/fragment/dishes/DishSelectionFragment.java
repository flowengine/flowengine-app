package ar.fiuba.tpprof.flowengine_restaurant.ui.fragment.dishes;

import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.domain.Order;
import ar.fiuba.tpprof.flowengine_restaurant.domain.OrderItem;
import ar.fiuba.tpprof.flowengine_restaurant.domain.meal.Dish;
import ar.fiuba.tpprof.flowengine_restaurant.ui.adapter.DishListAdapter;
import ar.fiuba.tpprof.flowengine_restaurant.ui.fragment.TakingOrderFlowFragment;

public abstract class DishSelectionFragment extends TakingOrderFlowFragment {

    protected ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public List<Dish> getSelectedDishes() {

        List<Dish> dishes = new ArrayList();
        SparseBooleanArray checked = listView.getCheckedItemPositions();

        for (int i = 0; i < listView.getCount(); i++){
            if (checked.get(i))
                dishes.add((Dish)listView.getAdapter().getItem(i));
        }
        return dishes;
    }

    protected void setListViewAdapter(DishListAdapter adapter){

        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                final int checkedCount = listView.getCheckedItemCount();
                mode.setTitle(checkedCount + " Seleccionados");
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.button_finish_dish_selection:
                        buildOrderItems();
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_dish_selection, menu);
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }
        });
    }

    protected void buildOrderItems(){
        List<Dish> dishesSelected = getSelectedDishes();
        Order order = takingOrderFlow.getOrder();

        for(Dish dish: dishesSelected){
            Integer quantity = 1;
            OrderItem orderItem = new OrderItem(dish, quantity, order);
            order.setOrderItem(orderItem);
        }
        mListener.onStepCompleted();
    }

}
