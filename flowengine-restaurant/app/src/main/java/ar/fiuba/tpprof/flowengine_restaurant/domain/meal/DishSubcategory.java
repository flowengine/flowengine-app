package ar.fiuba.tpprof.flowengine_restaurant.domain.meal;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import ar.fiuba.tpprof.flowengine_restaurant.domain.Entity;
import flowengine.annotations.injectable.PersistableModel;

/**
 * Created by florencia on 06/09/15.
 */
@PersistableModel
@DatabaseTable(tableName = "dish_subcategory")
public class DishSubcategory extends Entity implements ListItem {

    @DatabaseField(columnName = "name")
    private String name;

    public DishSubcategory(){}

    public DishSubcategory(String name){ this.name = name; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isSectionHeader(){
        return true;
    }

}
