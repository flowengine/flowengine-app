package ar.fiuba.tpprof.flowengine_restaurant.ui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.domain.meal.DishCategory;

/**
 * Created by florencia on 06/09/15.
 */
public class DishCategoryListAdapter extends ArrayAdapter<DishCategory> {

    private List<DishCategory> dishCategories;
    private Context context;

    public DishCategoryListAdapter(Context context, List<DishCategory> dishCategories) {
        super(context, R.layout.dish_category_list_item, dishCategories);
        this.dishCategories = dishCategories;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.dish_category_list_item , parent, false);
        setRowValues(position, rowView);

        return rowView;
    }

    private void setRowValues(int position, View rowView) {

        DishCategory dishCategory = dishCategories.get(position);

        TextView txtDishCategoryLabel = (TextView) rowView.findViewById(R.id.dish_category_item_name);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.dish_category_image);

        if (dishCategory.getName() != null)
            txtDishCategoryLabel.setText(dishCategory.getName());

        setDishCategoryIcon(imageView, dishCategory);
    }

    private void setDishCategoryIcon(ImageView imageView, DishCategory dishCategory){
        switch (dishCategory){
            case MAIN_COURSE:
                imageView.setImageResource(R.drawable.ic_main_course);
                break;
            case BEVERAGE:
                imageView.setImageResource(R.drawable.ic_beverage);
                break;
            case SNACK:
                imageView.setImageResource(R.drawable.ic_snack);
                break;
            case DESSERT:
                imageView.setImageResource(R.drawable.ic_dessert);
                break;
        }
    }
}
