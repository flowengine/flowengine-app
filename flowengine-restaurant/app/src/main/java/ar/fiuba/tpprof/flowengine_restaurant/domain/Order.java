package ar.fiuba.tpprof.flowengine_restaurant.domain;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.List;

import flowengine.annotations.injectable.PersistableModel;

/**
 * Created by florencia on 05/09/15.
 */
@PersistableModel
@DatabaseTable(tableName = "order")
public class Order extends Entity {

    public final static String TABLE_ID_FIELD_NAME = "table_id";

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = TABLE_ID_FIELD_NAME)
    private Table table;

    /* The order items that are persisted */
    @ForeignCollectionField
    private ForeignCollection<OrderItem> savedOrderItems;

    /* Temporary order items */
    private List<OrderItem> orderItems;

    public Order(){
        this.orderItems = new ArrayList<>();
    }

    public Order(Table table){
        this.table = table;
        this.orderItems = new ArrayList<>();
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public ForeignCollection<OrderItem> getSavedOrderItems() {
        return savedOrderItems;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public void setOrderItem(OrderItem orderItem) {
        orderItems.add(orderItem);
    }

}
