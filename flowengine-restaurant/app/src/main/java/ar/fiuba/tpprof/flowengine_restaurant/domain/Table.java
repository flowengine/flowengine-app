package ar.fiuba.tpprof.flowengine_restaurant.domain;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.List;

import flowengine.annotations.injectable.PersistableModel;

/**
 * Created by florencia on 05/09/15.
 */
@PersistableModel
@DatabaseTable(tableName = "restaurant_table")
public class Table extends Entity {

    @DatabaseField(columnName = "number")
    private Integer number;

    /* Temporary orders */
    private List<Order> orders;

    /* The orders that are persisted */
    @ForeignCollectionField
    private ForeignCollection<Order> savedOrders;

    public Table() { }

    public Table(Integer number) {
        this.number = number;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public ForeignCollection<Order> getSavedOrders() {
        return savedOrders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public void addOrder(Order order){
        this.orders.add(order);
    }

    public List<OrderItem> getOrdersItems(){
        List<OrderItem> orderItems = new ArrayList();
        if(savedOrders != null){
            for (Order order: savedOrders)
                orderItems.addAll(order.getSavedOrderItems());
        }
        return orderItems;
    }

    public Double getTotalCost(){
        Double total = 0d;
        if(savedOrders == null)
            return total;

        for (Order order: savedOrders)
            for(OrderItem orderItem: order.getSavedOrderItems())
                total += orderItem.getTotalCost();
        return total;
    }

}
