package ar.fiuba.tpprof.flowengine_restaurant.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import flowengine.FlowEngine;


public abstract class BaseFragment extends Fragment {

    protected String TAG = "BaseFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlowEngine.init(this, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        FlowEngine.stop(this);
    }

}
