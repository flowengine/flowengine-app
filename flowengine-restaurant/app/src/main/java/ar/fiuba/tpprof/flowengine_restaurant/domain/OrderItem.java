package ar.fiuba.tpprof.flowengine_restaurant.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import ar.fiuba.tpprof.flowengine_restaurant.domain.meal.Dish;
import flowengine.annotations.injectable.PersistableModel;

/**
 * Created by florencia on 08/09/15.
 */
@PersistableModel
@DatabaseTable(tableName = "order_item")
public class OrderItem extends Entity {

    public final static String ORDER_ID_FIELD_NAME = "order_id";
    public final static String DISH_ID_FIELD_NAME = "dish_id";

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = DISH_ID_FIELD_NAME)
    private Dish dish;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = ORDER_ID_FIELD_NAME)
    private Order order;

    @DatabaseField
    private Integer quantity;

    public OrderItem(){}

    public OrderItem(Dish dish, Integer quantity, Order order) {
        this.dish = dish;
        this.quantity = quantity;
        this.order = order;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Double getTotalCost(){
        if(getDish() == null || getDish().getCost() == null || getQuantity() == null)
            return 0d;
        return getDish().getCost() * getQuantity();
    }

    public void increaseQuantity(int value) {
        this.quantity += value;
    }

    public void decreaseQuantity(int value) {
        this.quantity -= value;
    }

}
