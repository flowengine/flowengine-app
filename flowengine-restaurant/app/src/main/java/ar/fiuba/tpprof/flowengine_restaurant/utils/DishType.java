package ar.fiuba.tpprof.flowengine_restaurant.utils;

/**
 * Created by florencia on 27/09/15.
 */
public class DishType {

    public final static String MAIN_COURSE = "mainCourse";
    public final static String BEVERAGE = "beverage";
    public final static String DESSERT = "dessert";
    public final static String SNACK = "snack";

}
