package ar.fiuba.tpprof.flowengine_restaurant.flow;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.ui.activity.TakingOrderActivity;
import ar.fiuba.tpprof.flowengine_restaurant.ui.fragment.PaymentFragment;
import ar.fiuba.tpprof.flowengine_restaurant.ui.fragment.TableOrdersListFragment;
import ar.fiuba.tpprof.flowengine_restaurant.ui.fragment.TableSelectionFragment;
import flowengine.Flow;
import flowengine.annotations.flow.FlowSteps;
import flowengine.annotations.flow.Sequence;
import flowengine.annotations.flow.StepContainer;
import flowengine.annotations.flow.methods.GetModel;
import flowengine.annotations.flow.methods.Jump;
import flowengine.annotations.flow.methods.Next;
import flowengine.annotations.flow.methods.SetModel;
import flowengine.annotations.flow.methods.WaitForResult;
import flowengine.annotations.flow.parameters.Argument;

/**
 * Created by florencia on 13/09/15.
 */
@FlowSteps(
        sequences = {
                @Sequence(id = "main", steps = {TableSelectionFragment.class, TableOrdersListFragment.class, PaymentFragment.class})
        })
@StepContainer(R.id.home_container)
public interface MainFlow extends Flow {

    public static final int NEW_ORDER = 0x1;

    @Next
    void next(@Argument("tableId") Long tableId);

    @Jump(TakingOrderActivity.class)
    @WaitForResult(NEW_ORDER)
    void jumpNewOrder(@Argument("tableId") Long tableId);

    @SetModel("tableId") void setCurrentTableId(Long tableId);
    @GetModel("tableId") long getCurrentTableId();

}
