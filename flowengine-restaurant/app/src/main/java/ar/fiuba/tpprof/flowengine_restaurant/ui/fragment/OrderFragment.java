package ar.fiuba.tpprof.flowengine_restaurant.ui.fragment;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;

import java.util.ArrayList;
import java.util.List;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.domain.Order;
import ar.fiuba.tpprof.flowengine_restaurant.domain.OrderItem;
import ar.fiuba.tpprof.flowengine_restaurant.ui.adapter.OrderListAdapter;

public class OrderFragment extends TakingOrderFlowFragment {

    private SwipeListView listOrderItemsView;
    private OrderListAdapter adapter;
    private List<OrderItem> orderItems;
    private Order order;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        this.order = takingOrderFlow.getOrder();
        this.orderItems = new ArrayList(order.getOrderItems());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_order, container, false);
        buildListView(view);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_new_order, menu);
        menu.findItem(R.id.button_add_order).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                order.setOrderItems(orderItems);
                takingOrderFlow.addOrderToTable(order);
                return true;
            }
        });

    }

    private void buildListView(View view) {

        adapter = new OrderListAdapter(getActivity(), orderItems);
        listOrderItemsView = (SwipeListView) view.findViewById(R.id.list_order);
        listOrderItemsView.setAdapter(adapter);
        listOrderItemsView.setSwipeListViewListener(new BaseSwipeListViewListener() {
            @Override
            public void onDismiss(int[] reverseSortedPositions) {
                for (int position : reverseSortedPositions) {
                    adapter.remove(adapter.getItem(position));
                }
            }

            @Override
            public void onOpened(int position, boolean toRight) {
                super.onOpened(position, toRight);
                listOrderItemsView.closeOpenedItems();
                OrderItem orderItem = adapter.getItem(position);
                if (toRight) {
                    orderItem.increaseQuantity(1);
                } else {
                    if (orderItem.getQuantity() > 1) {
                        orderItem.decreaseQuantity(1);
                    } else {
                        adapter.remove(orderItem);
                    }
                }
            }

            @Override
            public void onClosed(int position, boolean fromRight) {
                super.onClosed(position, fromRight);
                adapter.notifyDataSetChanged();
            }
        });

        listOrderItemsView.setAdapter(adapter);
    }
}
