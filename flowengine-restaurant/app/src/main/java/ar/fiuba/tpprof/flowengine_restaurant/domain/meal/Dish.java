package ar.fiuba.tpprof.flowengine_restaurant.domain.meal;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import ar.fiuba.tpprof.flowengine_restaurant.domain.Entity;
import flowengine.annotations.injectable.PersistableModel;

/**
 * Created by florencia on 05/09/15.
 */
@PersistableModel
@DatabaseTable(tableName="dish")
public class Dish extends Entity implements ListItem {

    public final static String SUBCAT_ID_FIELD_NAME = "dish_subcategory_id";
    public final static String DISH_TYPE_FIELD_NAME = "dish_type";

    @DatabaseField(columnName = "name")
    protected String name;

    @DatabaseField(columnName = "cost")
    protected Double cost;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true, columnName = SUBCAT_ID_FIELD_NAME)
    protected DishSubcategory dishSubcategory;

    @DatabaseField(columnName = DISH_TYPE_FIELD_NAME)
    protected String dishType;

    private boolean selected;

    public Dish(){}

    public Dish(String name, Double cost, DishSubcategory subcategory){
        this.name = name;
        this.cost = cost;
        this.dishSubcategory = subcategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public DishSubcategory getDishSubcategory() {
        return dishSubcategory;
    }

    public void setDishSubcategory(DishSubcategory dishSubcategory) {
        this.dishSubcategory = dishSubcategory;
    }

    public Boolean isSectionHeader(){
        return false;
    }

    public boolean isSelected(){
        return selected;
    }

    public void setSelected(boolean selected){
        this.selected = selected;
    }

    public String getDishType() {
        return dishType;
    }

    public void setDishType(String dishType) {
        this.dishType = dishType;
    }
}
