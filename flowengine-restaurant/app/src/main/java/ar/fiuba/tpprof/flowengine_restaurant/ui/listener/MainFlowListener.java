package ar.fiuba.tpprof.flowengine_restaurant.ui.listener;

/**
 * Created by florencia on 13/09/15.
 */
public interface MainFlowListener {

    void onStepCompleted(Long tableId);

    void changeTitle(String title);
}
