package ar.fiuba.tpprof.flowengine_restaurant.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.fortysevendeg.swipelistview.SwipeListView;

import java.util.List;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.domain.OrderItem;
import ar.fiuba.tpprof.flowengine_restaurant.domain.meal.Dish;

public class OrderListAdapter extends ArrayAdapter<OrderItem> {

    private List<OrderItem> orderItems;
    private Context context;

    public OrderListAdapter(Context context, List<OrderItem> orderItems) {
        super(context, R.layout.order_list_item, orderItems);
        this.orderItems = orderItems;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final OrderItem orderItem = orderItems.get(position);
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.order_list_item, parent, false);
            holder = new ViewHolder();
            holder.tvTitle = (TextView) convertView.findViewById(R.id.order_list_item_name);
            holder.tvCount = (TextView) convertView.findViewById(R.id.order_list_item_count);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ((SwipeListView)parent).recycle(convertView, position);

        Dish dish = orderItem.getDish();
        if (dish != null && dish.getName() != null)
            holder.tvTitle.setText(dish.getName());
        holder.tvCount.setText(Integer.toString(orderItem.getQuantity()));

        return convertView;
    }

    static class ViewHolder {
        TextView tvTitle;
        TextView tvCount;
    }
}
