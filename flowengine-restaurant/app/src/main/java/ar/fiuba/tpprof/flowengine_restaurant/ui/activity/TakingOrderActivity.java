package ar.fiuba.tpprof.flowengine_restaurant.ui.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.domain.Order;
import ar.fiuba.tpprof.flowengine_restaurant.domain.Table;
import ar.fiuba.tpprof.flowengine_restaurant.flow.TakingOrderFlow;
import ar.fiuba.tpprof.flowengine_restaurant.ui.listener.StepListener;
import flowengine.annotations.flow.FEActivity;
import flowengine.annotations.flow.Flow;
import flowengine.annotations.injectable.Model;
import flowengine.annotations.injectable.Persistable;
import flowengine.core.persistence.SQLDao;


@FEActivity(TakingOrderFlow.class)
public class TakingOrderActivity extends BaseActivity implements StepListener {

    private static final String TAG = "TakingOrderActivity";

    @Flow
    TakingOrderFlow takingOrderFlow;

    @Model("order")
    Order order;

    @Persistable
    SQLDao<Table,Long> daoTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taking_order);

        try {
            if(getIntent() != null){
                Long tableId = getIntent().getLongExtra("tableId",0);
                takingOrderFlow.setTable(daoTable.get(tableId));
            }
            if (order == null) {
                order = new Order(takingOrderFlow.getTable());
                takingOrderFlow.setOrder(order);
            }
        } catch (Exception ex){
            Log.e(TAG, ex.getMessage());
        }
    }

    @Override
    public void changeTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void onStepCompleted() {
        takingOrderFlow.addToOrder();
    }

    public void newDish(View view) {
        takingOrderFlow.next();
    }

}
