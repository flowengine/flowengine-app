package ar.fiuba.tpprof.flowengine_restaurant.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.domain.Order;
import ar.fiuba.tpprof.flowengine_restaurant.domain.OrderItem;
import ar.fiuba.tpprof.flowengine_restaurant.domain.Table;
import ar.fiuba.tpprof.flowengine_restaurant.flow.MainFlow;
import ar.fiuba.tpprof.flowengine_restaurant.ui.adapter.TableOrdersListAdapter;
import flowengine.FlowEngine;
import flowengine.annotations.injectable.Persistable;
import flowengine.core.persistence.SQLDao;

public class TableOrdersListFragment extends MainFlowFragment {

    private static final String TITLE = "Pedidos Mesa ";

    private ListView listView;
    private TableOrdersListAdapter adapter;

    private List<OrderItem> orderItems;
    private Table table;

    @Persistable
    SQLDao<Table,Long> daoTable;

    @Persistable
    SQLDao<Order,Long> daoOrder;

    @Persistable
    SQLDao<OrderItem,Long> daoOrderItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        try {
            Long tableId = ((MainFlow) FlowEngine.getFlow()).getCurrentTableId();
            this.table = daoTable.get(tableId);
            this.orderItems = new ArrayList<>();
        } catch (Exception ex){
            Log.e("ERROR", ex.getMessage());
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        this.orderItems.clear();
        this.orderItems.addAll(table.getOrdersItems());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setFragmentTitle(){
        mListener.changeTitle(TITLE + table.getNumber());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_table_orders_list, menu);
        menu.findItem(R.id.button_pay).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                payOrder();
                return true;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_table_orders_list, container, false);
        if(orderItems != null)
            buildListView(view);

        return view;
    }

    private void buildListView(View view) {

        adapter = new TableOrdersListAdapter(getActivity(), orderItems);
        listView = (ListView) view.findViewById(R.id.list_table_orders);
        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                final int checkedCount = listView.getCheckedItemCount();
                mode.setTitle(checkedCount + " Selected");
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.button_delete_order_item:
                        deleteOrderItems();
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_table_order_item_selection, menu);
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }
        });
    }

    public void payOrder(){
        mListener.onStepCompleted(table.getId());
    }

    private void deleteOrderItems(){

        List<OrderItem> orderItemsSelected = new ArrayList();
        SparseBooleanArray checked = listView.getCheckedItemPositions();

        for (int i = 0; i < listView.getCount(); i++){
            if (checked.get(i))
                orderItemsSelected.add(adapter.getItem(i));
        }

        for(OrderItem orderItem: orderItemsSelected){
            adapter.remove(orderItem);
            removeOrderItem(orderItem);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case MainFlow.NEW_ORDER:
                    Order order = (Order) data.getSerializableExtra("order");
                    saveTableOrder(order);
                    return;
                default:
                    super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private void saveTableOrder(Order order){
        try{
            order.setTable(table);
            daoOrder.save(order);
            for(OrderItem item: order.getOrderItems()) {
                item.setOrder(order);
                daoOrderItem.save(item);
            }
        } catch (Exception ex){
            Log.e(TAG, "Not possible to update table's orders. " + ex.getMessage());
        }
    }

    private void removeOrderItem(OrderItem orderItem){
        try{
            daoOrderItem.delete(orderItem);
        } catch (Exception ex){
            Log.e(TAG, "Not possible to delete order item. " + ex.getMessage());
        }
    }
}
