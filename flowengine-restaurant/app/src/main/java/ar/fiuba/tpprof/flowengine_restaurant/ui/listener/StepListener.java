package ar.fiuba.tpprof.flowengine_restaurant.ui.listener;

/**
 * Created by florencia on 05/09/15.
 */
public interface StepListener {

    void onStepCompleted();

    void changeTitle(String title);

}
