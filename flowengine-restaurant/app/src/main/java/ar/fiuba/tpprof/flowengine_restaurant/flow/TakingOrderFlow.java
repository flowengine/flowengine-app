package ar.fiuba.tpprof.flowengine_restaurant.flow;

import android.app.Activity;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.domain.Order;
import ar.fiuba.tpprof.flowengine_restaurant.domain.Table;
import ar.fiuba.tpprof.flowengine_restaurant.ui.fragment.OrderFragment;
import ar.fiuba.tpprof.flowengine_restaurant.ui.fragment.dishes.BeverageSelectionFragment;
import ar.fiuba.tpprof.flowengine_restaurant.ui.fragment.DishCategorySelectionFragment;
import ar.fiuba.tpprof.flowengine_restaurant.ui.fragment.dishes.DessertSelectionFragment;
import ar.fiuba.tpprof.flowengine_restaurant.ui.fragment.dishes.MainCourseSelectionFragment;
import ar.fiuba.tpprof.flowengine_restaurant.ui.fragment.dishes.SnackSelectionFragment;
import flowengine.Flow;
import flowengine.annotations.flow.FlowSteps;
import flowengine.annotations.flow.Join;
import flowengine.annotations.flow.Sequence;
import flowengine.annotations.flow.StepContainer;
import flowengine.annotations.flow.methods.BackStack;
import flowengine.annotations.flow.methods.Exit;
import flowengine.annotations.flow.methods.GetModel;
import flowengine.annotations.flow.methods.Next;
import flowengine.annotations.flow.methods.SetModel;
import flowengine.annotations.flow.parameters.Argument;

/**
 * Created by florencia on 05/09/15.
 */
@FlowSteps(
        sequences = {
                @Sequence(id = "start", steps = {DishCategorySelectionFragment.class}),
                @Sequence(id = "main", steps = {OrderFragment.class, DishCategorySelectionFragment.class}),
                @Sequence(id = "main_course", steps = {MainCourseSelectionFragment.class}, exitJoin=@Join(id = "main", stepIndex = 0)),
                @Sequence(id = "beverage", steps = {BeverageSelectionFragment.class}, exitJoin=@Join(id = "main", stepIndex = 0)),
                @Sequence(id = "snack", steps = {SnackSelectionFragment.class}, exitJoin=@Join(id = "main", stepIndex = 0)),
                @Sequence(id = "dessert", steps = {DessertSelectionFragment.class}, exitJoin=@Join(id = "main", stepIndex = 0))
        })
@StepContainer(R.id.taking_order_container)
public interface TakingOrderFlow extends Flow {

        @Next void next();

        @Next @BackStack(false) void addToOrder();

        @Next("main_course") void startMainCourseSequence();

        @Next("beverage") void startBeverageSequence();

        @Next("snack") void startSnackSequence();

        @Next("dessert") void startDessertSequence();

        @SetModel("order") void setOrder(Order order);

        @GetModel("order") Order getOrder();

        @SetModel("table") void setTable(Table table);

        @GetModel("table") Table getTable();

        @Exit(Activity.RESULT_OK) void addOrderToTable(@Argument("order") Order order);

        @Exit(Activity.RESULT_CANCELED) void cancelNewOrder();
}
