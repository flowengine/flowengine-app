package ar.fiuba.tpprof.flowengine_restaurant.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import flowengine.FlowEngine;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlowEngine.init(this, savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        FlowEngine.resume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        FlowEngine.pause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FlowEngine.back();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        FlowEngine.saveInstanceState(this, outState);
    }

    @Override
    public void onStop() {
        super.onStop();
        FlowEngine.stop(this);
    }

}
