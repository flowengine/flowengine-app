package ar.fiuba.tpprof.flowengine_restaurant.webservice;

import java.util.List;

import ar.fiuba.tpprof.flowengine_restaurant.domain.Table;
import ar.fiuba.tpprof.flowengine_restaurant.domain.meal.Dish;
import flowengine.WebResponse;
import flowengine.annotations.webservice.Cache;
import flowengine.annotations.webservice.Equals;
import flowengine.annotations.webservice.FEWebService;
import retrofit.http.GET;
import retrofit.http.Path;

@FEWebService
public interface TableWebService {

    @Cache
    @GET("/tables")
    void tables(WebResponse<List<Table>> callback);

    @Cache
    @GET("/dishes/{type}")
    void dishes(@Path("type") @Equals(Dish.DISH_TYPE_FIELD_NAME) String type, WebResponse<List<Dish>> callback);


}
