package ar.fiuba.tpprof.flowengine_restaurant.ui.fragment.dishes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.domain.meal.Dish;
import ar.fiuba.tpprof.flowengine_restaurant.ui.adapter.DishListAdapter;
import ar.fiuba.tpprof.flowengine_restaurant.utils.DishType;
import ar.fiuba.tpprof.flowengine_restaurant.webservice.TableWebService;
import flowengine.WebFailureEvent;
import flowengine.WebResponse;
import flowengine.WebSuccessEvent;
import flowengine.core.injection.BaseIoCContainer;

public class BeverageSelectionFragment extends DishSelectionFragment {

    public TableWebService tableWebService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BaseIoCContainer.getInstance().injectSingletons(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_beverage_selection, container, false);
        tableWebService.dishes(DishType.BEVERAGE, new WebResponse(new WebSuccessEventListBeverage(), new WebFailureEvent()));

        return view;
    }

    public class WebSuccessEventListBeverage extends WebSuccessEvent<List<Dish>> {}

    public void onEventMainThread(WebSuccessEventListBeverage event) {
        buildListView(getView(), event.get());
    }

    protected void buildListView(View view, List<Dish> beverages){

        DishListAdapter adapter = new DishListAdapter(getActivity(), beverages);
        listView = (ListView) view.findViewById(R.id.list_beverage);
        setListViewAdapter(adapter);
    }

}
