package ar.fiuba.tpprof.flowengine_restaurant.domain.meal;

/**
 * Created by florencia on 06/09/15.
 */
public interface ListItem {

    Boolean isSectionHeader();

}
