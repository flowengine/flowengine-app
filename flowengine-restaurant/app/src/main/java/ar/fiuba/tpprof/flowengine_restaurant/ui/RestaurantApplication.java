package ar.fiuba.tpprof.flowengine_restaurant.ui;

import android.app.Application;

import ar.fiuba.tpprof.flowengine_restaurant.webservice.TableWebService;
import flowengine.AppConfigurator;
import flowengine.FlowEngine;
import flowengine.core.webservice.WebServiceAdapter;

/**
 * Created by florencia on 05/09/15.
 */
public class RestaurantApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FlowEngine.initApp(new AppConfigurator() {

            @Override
            public void createWebAdapters() {
                WebServiceAdapter adapter = WebServiceAdapter.startBuilding()
                        .setEndpoint("http://192.168.0.24:9999")
                        .setContext(getApplicationContext())
                        .build();
                registerWebAdapter("test", adapter);
            }

            @Override
            public void createWebServices() {
                registerWebService("test", TableWebService.class);
            }

            @Override
            public void registerSingletons() {

            }
        });
    }
}
