package ar.fiuba.tpprof.flowengine_restaurant.ui.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.flow.MainFlow;
import ar.fiuba.tpprof.flowengine_restaurant.ui.listener.MainFlowListener;
import flowengine.annotations.flow.FEActivity;
import flowengine.annotations.flow.Flow;
import flowengine.annotations.injectable.Model;

@FEActivity(MainFlow.class)
public class HomeActivity extends BaseActivity implements MainFlowListener {

    private static final String TAG = "HomeActivity";

    @Flow
    MainFlow mainFlow;

    @Model("tableId")
    Long tableId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public void changeTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void onStepCompleted(Long tableId) {
        this.tableId = tableId;
        mainFlow.setCurrentTableId(tableId);
        mainFlow.next(tableId);
    }

    public void newOrder(View view){
        mainFlow.jumpNewOrder(tableId);
    }

}
