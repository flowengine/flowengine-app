package ar.fiuba.tpprof.flowengine_restaurant.domain;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

/**
 * Created by florencia on 05/09/15.
 */
public abstract class Entity implements Serializable {

    @DatabaseField(generatedId = true)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
