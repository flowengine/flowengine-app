package ar.fiuba.tpprof.flowengine_restaurant.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.domain.Table;

/**
 * Created by florencia on 05/09/15.
 */
public class TableListAdapter extends ArrayAdapter<Table> {

    private final static String TABLE_LABEL = "Mesa ";
    private List<Table> tables;
    private Context context;

    public TableListAdapter(Context context, List<Table> tables) {
        super(context, R.layout.table_list_item,tables);
        this.tables = tables;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.table_list_item , parent, false);
        setRowValues(position, rowView);

        return rowView;
    }

    private void setRowValues(int position, View rowView) {

        Table table = tables.get(position);

        TextView txtTableLabel = (TextView) rowView.findViewById(R.id.table_list_item_number);

        if (table.getNumber() != null)
            txtTableLabel.setText(TABLE_LABEL + table.getNumber().toString());

    }
}
