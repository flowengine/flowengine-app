package ar.fiuba.tpprof.flowengine_restaurant.ui.fragment;

import android.app.Activity;
import android.os.Bundle;

import ar.fiuba.tpprof.flowengine_restaurant.domain.Table;
import ar.fiuba.tpprof.flowengine_restaurant.flow.TakingOrderFlow;
import ar.fiuba.tpprof.flowengine_restaurant.ui.listener.StepListener;
import flowengine.annotations.flow.Flow;

public abstract class TakingOrderFlowFragment extends BaseFragment {

    private static final String DEFAULT_TITLE = "Nuevo Pedido Mesa ";

    protected StepListener mListener;

    @Flow
    protected TakingOrderFlow takingOrderFlow;

    protected Table table;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.table = takingOrderFlow.getTable();
    }

    @Override
    public void onResume(){
        super.onResume();
        setFragmentTitle();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (StepListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement StepListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setFragmentTitle(){
        if(table != null)
            mListener.changeTitle(DEFAULT_TITLE + table.getNumber());
    }

}
