package ar.fiuba.tpprof.flowengine_restaurant.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.domain.Table;
import ar.fiuba.tpprof.flowengine_restaurant.ui.adapter.TableListAdapter;
import ar.fiuba.tpprof.flowengine_restaurant.webservice.TableWebService;
import flowengine.WebFailureEvent;
import flowengine.WebResponse;
import flowengine.WebSuccessEvent;
import flowengine.core.injection.BaseIoCContainer;

public class TableSelectionFragment extends MainFlowFragment {

    private static String TAG = "TableSelectionFragment";

    private ListView listTableView;
    public TableWebService tableWebService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BaseIoCContainer.getInstance().injectSingletons(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_table_selection, container, false);
        tableWebService.tables(new WebResponse(new WebSuccessEventListTable(), new WebFailureEvent()));
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onEvent(WebFailureEvent webError) {
        Log.d(TAG, "failure retrofit " + webError.get().getUrl() + " " + webError.get().getKind());
    }

    public void onEventMainThread(WebSuccessEventListTable event) {
        buildListView(getView(), event.get());
    }

    public class WebSuccessEventListTable extends WebSuccessEvent<List<Table>> {}

    private void buildListView(View view, List<Table> tables) {

        TableListAdapter adapter = new TableListAdapter(getActivity(), tables);
        listTableView = (ListView) view.findViewById(R.id.list_table);
        listTableView.setAdapter(adapter);

        listTableView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                Table table = (Table) listTableView.getItemAtPosition(position);
                mListener.onStepCompleted(table.getId());
            }
        });
    }

}
