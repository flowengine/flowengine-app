package ar.fiuba.tpprof.flowengine_restaurant.domain.meal;

/**
 * Created by florencia on 06/09/15.
 */
public enum DishCategory {

    SNACK("Snack"), MAIN_COURSE("Plato Principal"),
    DESSERT("Postre"), BEVERAGE("Bebida");

    private final String name;

    DishCategory(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
