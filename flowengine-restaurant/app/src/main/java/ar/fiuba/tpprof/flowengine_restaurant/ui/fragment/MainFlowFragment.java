package ar.fiuba.tpprof.flowengine_restaurant.ui.fragment;


import android.app.Activity;

import ar.fiuba.tpprof.flowengine_restaurant.ui.listener.MainFlowListener;

public abstract class MainFlowFragment extends BaseFragment {

    private static final String DEFAULT_TITLE = "Restaurant";

    protected MainFlowListener mListener;

    @Override
    public void onResume(){
        super.onResume();
        setFragmentTitle();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (MainFlowListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement MainFlowListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setFragmentTitle(){
        mListener.changeTitle(DEFAULT_TITLE);
    }

}
