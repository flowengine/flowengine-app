package ar.fiuba.tpprof.flowengine_restaurant.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.domain.OrderItem;
import ar.fiuba.tpprof.flowengine_restaurant.domain.Table;
import ar.fiuba.tpprof.flowengine_restaurant.ui.adapter.PaymentListAdapter;
import flowengine.annotations.injectable.Persistable;
import flowengine.core.persistence.SQLDao;

public class PaymentFragment extends MainFlowFragment {

    private static final String TITLE = "Pago Mesa ";

    private ListView listView;
    private PaymentListAdapter adapter;

    private List<OrderItem> orderItems;
    private Table table;

    @Persistable
    SQLDao<Table,Long> daoTable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            Long tableId = this.getArguments().getLong("tableId");
            this.table = daoTable.get(tableId);
            this.orderItems = table.getOrdersItems();
        } catch (Exception ex){
            Log.e("ERROR", ex.getMessage());
        }
    }

    @Override
    public void setFragmentTitle(){
        mListener.changeTitle(TITLE + table.getNumber());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_payment, container, false);
        View footerView = inflater.inflate(R.layout.payment_total, container, false);
        buildListView(view, footerView);
        return view;
    }

    private void buildListView(View view, View footerView) {

        adapter = new PaymentListAdapter(getActivity(), orderItems);
        listView = (ListView) view.findViewById(R.id.payment_list);
        listView.setAdapter(adapter);
        listView.addFooterView(footerView);

        TextView txtSubTotal = (TextView) view.findViewById(R.id.subtotal_payment);
        TextView txtIva = (TextView) view.findViewById(R.id.payment_iva);
        TextView txtTotal = (TextView) view.findViewById(R.id.payment_total);

        Double totalCost = table.getTotalCost();
        Double iva = 21.0;
        Double total = totalCost + (totalCost * iva) / 100;
        txtSubTotal.setText(totalCost.toString());
        txtIva.setText(iva.toString());
        txtTotal.setText(total.toString());

    }

}
