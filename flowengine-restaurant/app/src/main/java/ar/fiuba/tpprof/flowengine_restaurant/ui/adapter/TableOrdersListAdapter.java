package ar.fiuba.tpprof.flowengine_restaurant.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ar.fiuba.tpprof.flowengine_restaurant.R;
import ar.fiuba.tpprof.flowengine_restaurant.domain.OrderItem;
import ar.fiuba.tpprof.flowengine_restaurant.domain.meal.Dish;

/**
 * Created by florencia on 13/09/15.
 */
public class TableOrdersListAdapter extends ArrayAdapter<OrderItem> {

    private List<OrderItem> orderItems;
    private Context context;

    public TableOrdersListAdapter(Context context, List<OrderItem> orderItems) {
        super(context, R.layout.table_orders_list_item, orderItems);
        this.orderItems = orderItems;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.table_orders_list_item, parent, false);
        setRowValues(position, rowView);

        return rowView;
    }

    private void setRowValues(int position, View rowView) {

        OrderItem orderItem = orderItems.get(position);
        TextView txtNameLabel = (TextView) rowView.findViewById(R.id.order_item_name);
        TextView txtQuantityLabel = (TextView) rowView.findViewById(R.id.order_item_quantity);

        Dish dish = orderItem.getDish();
        if (dish != null && dish.getName() != null)
            txtNameLabel.setText(dish.getName());

        if (orderItem.getQuantity() != null)
            txtQuantityLabel.setText(orderItem.getQuantity().toString());

    }

}
